# OSA Front End developer #

El siguiente repositorio, corresponde a un desafío que te proponemos como OSA para poder ser parte nuestro equipo de desarrollado.

## Que empiece la acción ##

**Problema**: Se necesita una aplicación _single page_ con información de películas estrenadas o anunciadas.

* En la página principal añade un buscador que permite filtrar las películas a mostrar
* Una vez que tengas información suficiente muestra un listado de películas, por cada película se muestra al menos una imagen y el título
* Al hacer click sobre una película, se debe mostrar el detalle de ésta: director, actores, etc

## Instrucciones ##
1. Haz un fork a este repositorio.
2. Escribe tu código en la rama **_develop_**.
3. Cuando estés seguro de tus cambios y hayas finalizado el código, haz un pull request a este repositorio.
4. [OMDb](http://www.omdbapi.com/) ofrece una API con las películas existentes en el mercado.

## Debes ##

* Usar al menos react, redux, react-route, react-thunk/saga y axios.
* Utilizar sass o less en los estilos personalizados.

## Opcional ##

* Realizar test a los componentes y/o stores. (te recomendamos jest y tape).
* Despliega la aplicación en algún servidor cloud como [AWS](https://aws.amazon.com/es/), [Google Cloud](https://cloud.google.com/) o [Heroku](http://heroku.com/)


## Consejos ##

* Plantear tu propio diseño.